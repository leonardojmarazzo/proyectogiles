<?php

require __DIR__.'/models/Producto.php';


function calcularDisponibles(){

    $codigosComercial = [
        '397969',
        '397981',
        '401183',
        '397092',
        '397509',
        '507144',
        '507147',
        '507101',
        '507097',
        '507093',
        '401149',
        '507190',
        '507110',
        '397000'
    ];

    $filename = "disponibles" . date('Ymd') . ".xls";

    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");
    $fecha = date('Y-m-d');
    echo "Codigo" . "\t" . "Descripcion" . "\t" . "Disponible\r\n";
    foreach ($codigosComercial as $codigo) {
        $prod = new Producto($codigo);
        $disp = intval($prod->getDisponiblesDestino($fecha, "Comercial"));
        if ($disp != 0){
            $tot = $codigo . "\t" . $prod->getDescripcion() . "\t" . $disp . "\r\n";
            echo $tot;
        }
    }
}

session_start();

if (isset($_SESSION['perfil'])){
    if ($_SESSION['perfil'] == 'PCP' || $_SESSION['perfil'] == 'Administrador' || $_SESSION['perfil'] == 'comercial'){
        calcularDisponibles();
    }else{
        Header("Location: ../main.php");
    }
}else{
    require_once __DIR__.'/views/login.html';
}