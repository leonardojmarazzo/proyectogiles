<?php

require __DIR__.'/../models/Usuario.php';



$username = $_POST['user'];
$password = $_POST['pass'];

$user = new Usuario($username);

if (password_verify($password, $user->getHash())){
    session_start();
    $_SESSION['user'] = $username; 
    $_SESSION['perfil'] = $user->getPerfil();
    echo $user->getPerfil();
}else{
    echo 'invalid';
}
