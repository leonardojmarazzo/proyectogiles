<?php


require __DIR__.'/../models/Usuario.php';

$passVieja = $_POST['antigua'];
$passNueva = $_POST['nueva'];

$hash = password_hash($passNueva, PASSWORD_DEFAULT);

session_start();

$usuario = $_SESSION['user'];

$user = new Usuario($usuario);

if (password_verify($passVieja, $user->getHash())){
    $user->cambiarContraseña($hash);
    echo 'Guardado';
}else{
    echo 'invalid';
}