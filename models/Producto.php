<?php

require __DIR__.'./../config/PdoFactory.php';

class Producto
{
    public $Codigo;
    public $Descripcion;
    public $Tipo;
    public $Familia;
    public $RendimientoHistorico;
    private $campos = ['Codigo', 'Descripcion', 'Tipo', 'Familia', 'Rendimientohistorico'];


    

    public function __Construct($cod, $des = '', $tipo = '', $familia = '', $Rendimiento = ''){
        $this->Codigo = $cod;
        $this->Descripcion = $des;
        $this->Tipo = $tipo;
        $this->Familia = $familia;
        if ($Rendimiento == ''){
            $this->buscarRindeHist();
        }
        
    }

    public function getDescripcion(){
        if ($this->Descripcion == ''){
            $pdo = PdoFactory::build();
            $sql = "SELECT * from Productos where Codigo = '" . $this->Codigo. "'";
            $query = $pdo->prepare($sql);
            $query->execute();
            $row = $query->fetch();
            $this->Descripcion = $row['Descripcion'];
        }
        return $this->Descripcion;
    }


    public function buscarRindeHist(){
        $pdo = PdoFactory::build();
        $sql = "SELECT * from Productos where Codigo = '" . $this->Codigo. "'";
        $query = $pdo->prepare($sql);
        $query->execute();

        $row = $query->fetch();
        $this->RendimientoHistorico = floatval($row['Rendimiento Historico']);
    }

    public function getDisponibles($fecha){
        $pdo = PdoFactory::build();
        $sql = "SELECT * from disponiblestotales where Fecha = '" . $fecha. "' and Codigo = '$this->Codigo'";
        $query = $pdo->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 1){
            $row = $query->fetch();
            array($row['Cantidad'], $row['Unidad']);
        }else{
            return $this->calcularDisponibles($fecha);
        }
    }

    public function getDisponiblesDestino($fecha, $destino){
        $pdo = PdoFactory::build();
        $this->getDisponibles($fecha);
        $disp = 0;
        $sql = '';
        $un = 'KG';
        if ($destino == 'Giles'){
            $sql = "SELECT * from disponiblestotales where codigo = '$this->Codigo' and fecha = '$fecha'";
            $query = $pdo->prepare($sql);
            $query->execute();
            $row = $query->fetch();
            $disp = $row['Cantidad'];
        }else if ($destino == "Varela"){
            if ($this->Codigo == '507201'){
                $padre = new Producto('507202');
                $disp = $padre->getDisponiblesDestino($fecha, 'Varela') - $padre->getEstimado($fecha, 'Varela');
            }elseif ($this->Codigo == '396956'){
                $padre = new Producto('507201');
                $disp = $padre->getDisponiblesDestino($fecha, 'Varela') - $padre->getEstimado($fecha, 'Varela');
            }elseif ($this->Codigo == '501482'){
                $padre = new Producto('396956');
                $disp = ($padre->getDisponiblesDestino($fecha, 'Varela') - $padre->getEstimado($fecha, 'Varela')) * 0.45;
            }else{
                $disp = $this->getDisponiblesDestino($fecha, 'Giles') - $this->getEstimado($fecha, 'Giles');
            }

        }else if ($destino == "Pilar"){
            if ($this->Codigo == '396956'){
                $padre = new Producto('581482');
                $disp = $padre->getDisponiblesDestino($fecha, 'Varela') - $padre->getEstimado($fecha, 'Varela');
            }elseif ($this->Codigo == '507203'){
                $padre = new Producto('396956');
                $disp = ($padre->getDisponiblesDestino($fecha, 'Pilar') - $padre->getEstimado($fecha, 'Pilar')) * 0.45;
            }else{
                $disp = $this->getDisponiblesDestino($fecha, 'Varela') - $this->getEstimado($fecha, 'Varela');
            }
        }else if ($destino == "Comercial"){
            $animales = $this->getAnimales();
            if ($this->Codigo == '397969'){
                $sql = "SELECT * from productos where (Descripcion like '%Panceta%') and (Tipo = 'CAP/MEI')";
                $query = $pdo->prepare($sql);
                $query->execute();
                $sum = 0;
                while ($row = $query->fetch()){
                    $panceta = new Producto($row['Codigo']);
                    $sum = $sum + $panceta->getEstimado($fecha, 'Varela') + $panceta->getEstimado($fecha, 'Pilar');
                }
                $disp = $animales * 2 - $sum;
                $un = 'UN';
            }elseif ($this->Codigo == '397981'){
                $bondio = new Producto('507104');
                $disp = $animales * 2 - $bondio->getEstimado($fecha, 'Varela');
                $un = 'UN';
            }elseif ($this->Codigo == '401183') {
                $carrevar = new Producto('507204');
                $carrepil = new Producto('507096');
                $disp = $animales * 2 - $carrepil->getEstimado($fecha, 'Pilar') - $carrevar->getEstimado($fecha, 'Varela');
                $un = 'UN';
            }elseif ($this->Codigo == '397092') {
                $carrevar = new Producto('507223');
                $carrepil = new Producto('507518');
                $disp = ($carrepil->getEstimado($fecha, 'Pilar') - $carrevar->getEstimado($fecha, 'Pilar'))*0.7;
            }elseif ($this->Codigo == '401183') {
                $carrevar = new Producto('507204');
                $carrepil = new Producto('507096');
                $disp = ($carrepil->getEstimado($fecha, 'Pilar') + $carrevar->getEstimado($fecha, 'Varela'))*0.4;
            }elseif ($this->Codigo == '507097') {
                $pil = new Producto('507207');
                $disp = $pil->getDisponiblesDestino($fecha, 'Pilar') - $pil->getEstimado($fecha, 'Pilar');
            }elseif ($this->Codigo == '401149' || $this->Codigo == '507190') {
                $disp = $animales * 0.75;
                $un = 'UN';
            }elseif ($this->Codigo == '397000') {
                $carrevar = new Producto('507204');
                $disp =  $carrevar->getEstimado($fecha, 'Varela') * 0.44;
                $un = 'UN';
            }elseif ($this->Codigo == '397070' || $this->Codigo == '397005' || $this->Codigo == '507122' || $this->Codigo == '397509' || $this->Codigo == '507144' 
                || $this->Codigo == '507147' || $this->Codigo == '507101' || $this->Codigo == '507097' || $this->Codigo == '507110') {
                $disp = $this->getDisponiblesDestino($fecha, 'Pilar') - $this->getEstimado($fecha, 'Pilar');
            }
        }
        $sql = "SELECT * from disponiblesxdestino where codigo = '$this->Codigo' and fecha = '$fecha' and Destino = '$destino'";
        $query = $pdo->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0){
            $sql = "UPDATE disponiblesxdestino set Cantidad = $disp, Unidad = $un where codigo = '$this->Codigo' and fecha = '$fecha' and Destino = '$destino'";
        }else {
            $sql = "INSERT into disponiblesxdestino (`Codigo`, `Destino`, `Fecha`, `Cantidad`, `Unidad`) values ('$this->Codigo', '$destino',  '$fecha', $disp, '$un')";
        }
        $query = $pdo->prepare($sql);
        $query->execute();
        return $disp;
    }


    public function getEstimado($fecha, $destino){
        $pdo = PdoFactory::build();
        //$date = date_format($fecha,  'Y-M-D');
        $sql = "SELECT Estimado from disponiblesxdestino where codigo = '$this->Codigo' and fecha = '$fecha' and Destino = '$destino'";
        $query = $pdo->prepare($sql);
        $query->execute();
        $row = $query->fetch();
        return $row['Estimado'];
    }

    public function setEstimado($fecha, $destino, $estimado){
        $pdo = PdoFactory::build();
        //$date = date_format($fecha,  'Y-M-D');
        $sql = "UPDATE disponiblesxdestino set Estimado = $estimado where codigo = '$this->Codigo' and fecha = '$fecha' and Destino = '$destino'";
        $query->execute();
    }


    public function getAnimales(){
        $animales = 0;
        $pdo = PdoFactory::build();
        $sql = "SELECT * from tropas where despostada = 0 and Categoria = 'CAP/MEI'";
        $query = $pdo->prepare($sql);
        $query->execute();
        while ($row = $query->fetch()){
            $animales = $animales + $row['Cantidad'];
        }
        return $animales;
    }

    public function getChanchas(){
        $animales = 0;
        $pdo = PdoFactory::build();
        $sql = "SELECT * from tropas where despostada = 0 and Categoria = 'CHA/PA'";
        $query = $pdo->prepare($sql);
        $query->execute();
        while ($row = $query->fetch()){
            $animales = $animales + $row['Cantidad'];
        }
        return $animales;
    }

    public function getDisponiblesChanchas(){
        $pdo = PdoFactory::build();
        $animales = $this->getChanchas();
        $disp = $animales * $this->RendimientoHistorico;
        $sql =  "INSERT into dispobiblestotales (`Codigo`, `Fecha`, `Cantidad`, `Unidad`) values ('$this->Codigo',  '$date', $disp, 'UN')";
        $query = $pdo->prepare($sql);
        $query->execute();
        return $disp;
    }

    public function calcularDisponibles($fecha){
        $animales = $this->getAnimales();
        $pdo = PdoFactory::build();
        

        $sql = "SELECT * from historicofaena";
        $query = $pdo->prepare($sql);
        $query->execute();


        $cant = 0;
        $sum = 0;
        while ($row = $query->fetch()){
            $sum = $sum + $row['PesoPromedioRes'];
            $cant = $cant + 1;
        }

        $promres = $sum / $cant;
        $promres;
        $disp = $animales * $promres * $this->RendimientoHistorico;
        $sql =  "INSERT into disponiblestotales (`Codigo`, `Fecha`, `Cantidad`, `Unidad`) values ('$this->Codigo',  '$fecha', $disp, 'KG')";
        $query = $pdo->prepare($sql);
        $query->execute();
        return $sql;
    }
}
