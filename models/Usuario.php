<?php

require __DIR__.'./../config/PdoFactory.php';

class Usuario
{
    public $username;
    private $hash;
    private $perfil;

    public function __Construct($username, $hash="", $perfil = ""){
        $this->username = $username;
        $this->hash = $hash;
        $this->perfil = $perfil;
    }


    /**
     * Get the value of hash
     */ 
    public function getHash()
    {
        $pdo = PdoFactory::build("usuarios");
        $uname = $this->username;
        $sql = "SELECT * from usuarios where username = '$uname'";
        $query = $pdo->prepare($sql);
        $query->execute();
        $row = $query->fetch();
        return $row['Hash'];
    }

    /**
     * Get the value of perfil
     */ 
    public function getPerfil()
    {
        $pdo = PdoFactory::build("usuarios");
        $sql = "SELECT perfil from usuarios where username = '$this->username'";
        $query = $pdo->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0){
            $row = $query->fetch();
            return $row['perfil'];
        }else{
            return 'invalid';
        }
        
    }



    public function guardar(){
        $pdo = PdoFactory::build("usuarios");
        $sql = "INSERT into usuarios (`username`, `Hash`, `Perfil`) values ('$this->username', '$this->hash', '$this->perfil')";
        $query = $pdo->prepare($sql);
        $query->execute();
    }


    public function cambiarContraseña($hash){
        $pdo = PdoFactory::build("usuarios");
        $sql = "UPDATE usuarios set `Hash` = '$hash' where `username` = '$this->username'";
        $query = $pdo->prepare($sql);
        $query->execute();
    }
}
