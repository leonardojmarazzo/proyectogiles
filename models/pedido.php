<?php

require __DIR__.'./../config/PdoFactory.php';

class Pedido
{
    public $id;
    public $fecha;
    public $destino;
    public $comercial;
    public $cliente;
    public $sql;


    public function __Construct($fecha, $destino = 'Comercial', $comercial = '', $cliente = ''){
        $pdo = PdoFactory::build();
        $this->sql = "SELECT max(idPedido) as id from pedidos where fecha = '$fecha'";
        $query = $pdo->prepare($this->sql);
        $query->execute();
        $row = $query->fetch();
        if ($row['id'] != ''){
            $this->id = $row['id'] + 1;
        }else{
            $this->id = 1;
        }
        $this->fecha = $fecha;
        $this->destino = $destino;
        $this->comercial = $comercial;
        $this->cliente = $cliente;
    }

    public function guardar(){
        
        $pdo = PdoFactory::build();
        $sql = "SELECT * from pedidos where fecha = '$this->fecha' and idPedido = $this->id";
        $query = $pdo->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0){
            
            $sql = "UPDATE pedidos set Destino = $this->destino, comercial = $this->comercial, Cliente = $this->cliente  where fecha = '$this->fecha' and idPedido = $this->id";
        }else{
            
            $sql = "INSERT into pedidos (`idPedido`, `Fecha`, `Destino`, `Comercial`, `Cliente`) values ($this->id, '$this->fecha', '$this->destino', '$this->comercial', '$this->cliente')";
        }
        $query = $pdo->prepare($sql);
        $query->execute();
        return $sql;
    }

    public function getProductos(){
        $prods = array();
        $date = date_format($this->fecha,  'Y-M-D');
        $pdo = PdoFactory::build();
        $sql = "SELECT `Producto`, `Descripcion`, `Cantidad`,  `Unidad`, `PrecioXKG`,  from productosxpedido as pp inner join productos as p on p.Codigo = pp.Producto  where fecha = '$date' and idPedido = $id";
        $query = $pdo->prepare($sql);
        $query->execute();
        while ($row = $query->fetch()){
            $prod = array();
            $prod['Codigo'] = $row['Producto'];
            $Prod['Descripcion'] = $row['Descripcion'];
            $prod['Cantidad'] = $row['Cantidad'];
            $prod['Unidad'] = $row['Unidad'];
            $prod['Precio'] = $row['PrecioxKG'];
            array_push($prods, $prod);
        }
        return $prods;
    }




    

}
