<?php


session_start();

if (isset($_SESSION['perfil'])){
    if ($_SESSION['perfil'] == 'faena' || $_SESSION['perfil'] == 'Administrador'){
        $perfil = $_SESSION['perfil'];
        $usuario = $_SESSION['user'];
        require_once __DIR__.'/views/tropas.php';
    }else{
        Header("Location: ../main.php");
    }
}else{
    require_once __DIR__.'/views/login.html';
}