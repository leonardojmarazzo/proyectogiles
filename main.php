<?php

session_start();

if (isset($_SESSION['perfil'])){
    $perfil = $_SESSION['perfil'];
    $usuario = $_SESSION['user'];
    require_once __DIR__.'/views/main.php';
}else{
    require_once __DIR__.'/views/login.html';
}