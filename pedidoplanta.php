<?php

session_start();

if (isset($_SESSION['perfil'])){
    $perfil = $_SESSION['perfil'];
    $usuario = $_SESSION['user'];
    if ($_SESSION['perfil'] == 'Administrador'){
        require_once __DIR__.'/views/pedidoplanta.php';
    }elseif ($_SESSION['perfil'] == 'Pilar' && $_GET['destino'] == 'Pilar'){
        require_once __DIR__.'/views/pedidoplanta.php';
    }elseif ($_SESSION['perfil'] == 'Varela' && $_GET['destino'] == 'Varela') {
        require_once __DIR__.'/views/pedidoplanta.php';
    }elseif ($_SESSION['perfil'] == 'Giles' && $_GET['destino'] == 'Giles') {
        require_once __DIR__.'/views/pedidoplanta.php';
    }else{
        Header("Location: ../main.php");
    }
}else{
    require_once __DIR__.'/views/login.html';
}