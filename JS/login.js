function login() {
    var xmlhttp = new XMLHttpRequest();
    var user = document.getElementById('uname').value;
    var pass = document.getElementById('pass').value;
    console.log(user + " " + pass);
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
             if (this.response == 'invalid'){
                 alert("Usuario o contraseña incorrectos");
             }else{
                 console.log(this.response);
                 window.location.href = "../main.php";
             }
        }
    };
    xmlhttp.open("POST", "controllers/loginController.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("user="+user+"&pass="+pass);
}


function cancel(){
    document.getElementById('uname').value = "";
    document.getElementById('pass').value = "";
}