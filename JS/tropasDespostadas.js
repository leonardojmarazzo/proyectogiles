function enter(event) {
    if (event.keyCode == 13 || event.which == 13) {
        var table = document.getElementById("tropas");
        var i = document.getElementById("tropas").rows.length;
        var row = table.insertRow(i);
        var cell1 = row.insertCell(0);
        cell1.innerHTML = '<input type="text" onkeypress="enter(event)">';
    }
}


function guardarTropas() {
    var table = document.getElementById("tropas");
    var len = document.getElementById("tropas").rows.length;
    var fecha = new Date().toISOString().substring(0, 10);
    for (i = 1; i < len - 2; i++) {
        var row = table.rows[i];
        var tropa = row.cells[0].children[0].value;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.response);
            }
        };
        xmlhttp.open("POST", "../controllers/tropasDesposte.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("fecha=" + fecha + "&tropa=" + tropa);
    }
}
