// When the user clicks on the button, open the modal 
function mostrar() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.response);
            if (this.response == "Administrador") {
                var modal = document.getElementById('myModal');
                modal.style.display = "block";
            }
        }
    };
    xmlhttp.open("GET", "../controllers/getPerfil.php", true);
    xmlhttp.send();
}

function mostrar2() {
    var modal = document.getElementById('myModal2');
    modal.style.display = "block";
}



// When the user clicks on <span> (x), close the modal
function cerrar() {
    var modal = document.getElementById('myModal');
    var modal2 = document.getElementById('myModal2');
    modal.style.display = "none";
    modal2.style.display = "none";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    var modal = document.getElementById('myModal');
    var modal2 = document.getElementById('myModal2');
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if (event.target == modal2) {
        modal2.style.display = "none";
    }
}

function crearUsuario() {
    var usuario = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;
    var perfil = document.getElementById("Perfil").value;
    document.getElementById("user").value = "";
    document.getElementById("pass").value = "";
    document.getElementById("Perfil").value = "";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.response);
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
    };
    xmlhttp.open("POST", "../controllers/crearUsuario.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("user=" + usuario + "&pass=" + pass + "&perfil=" + perfil);

}


function cambiarPass() {
    var pass = document.getElementById("passactual").value;
    var pass1 = document.getElementById("passnueva").value;
    var pass2 = document.getElementById("passnueva2").value;

    if (pass1 != pass2) {
        alert("Las contraseñas no coinciden");
    } else {

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.response == "invalid") {
                    alert("Contraseña actual incorrecta");
                } else {
                    document.getElementById("passactual").value = "";
                    document.getElementById("passnueva").value = "";
                    document.getElementById("passnueva2").value = "";
                    var modal = document.getElementById('myModal2');
                    modal.style.display = "none";
                    window.location.href = "../logoff.php";
                }
            }
        };
        xmlhttp.open("POST", "../controllers/cambiarContraseña.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("antigua=" + pass + "&nueva=" + pass1);
    }
}