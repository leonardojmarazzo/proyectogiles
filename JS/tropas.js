function enter(event) {
    if (event.keyCode == 13 || event.which == 13) {
        nuevaFila();
    }
}

function nuevaFila(){
    var table = document.getElementById("tropas");
        var i = document.getElementById("tropas").rows.length - 2;
        var row = table.insertRow(i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);
        var cell7 = row.insertCell(6);
        cell1.innerHTML = '<input type="date">';
        cell2.innerHTML = '<input type="text">';
        cell3.innerHTML = '<select name="Usuario"> <option value="Campo">CampoAustral</option> <option value="Tercero">Tercero</option></select>';
        cell4.innerHTML = '<input type="text">';
        cell5.innerHTML = '<select name="Usuario"><option value="CAP/MEI">CAP/MEI</option> <option value="CHA/PA">CHA/PA</option></select>';
        cell6.innerHTML = '<input type="number">';
        cell7.innerHTML = '<input type="number" onkeypress="enter(event)">';
}

function guardarTropas() {
    var table = document.getElementById("tropas");
    var len = document.getElementById("tropas").rows.length;
    for (i = 1; i < len - 2; i++) {
        var row = table.rows[i];
        var fecha = row.cells[0].children[0].value;
        var tropa = row.cells[1].children[0].value;
        var usuario = row.cells[2].children[0].value;
        var proveedor = row.cells[3].children[0].value;
        var categoria = row.cells[4].children[0].value;
        var cantidad = row.cells[5].children[0].value;
        var pesovivo = row.cells[6].children[0].value;



        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.response);
            }
        };
        xmlhttp.open("POST", "../controllers/tropasController.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("fecha=" + fecha + "&tropa=" + tropa + "&usuario=" + usuario + "&proveedor=" + proveedor + "&categoria=" + categoria + "&cantidad=" + cantidad + "&pesovivo=" + pesovivo);
    }
    alert("Guardado Correctamente");
    window.location.href = "../main.php";
}
