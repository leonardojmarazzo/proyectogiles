var planta = "";

function load() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("destino");
    planta = c;
    i = 1;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const resultado = JSON.parse(this.response);
            const table = document.getElementById("disponible");
            //console.log()

            resultado.forEach(function (item) {
                var row = table.insertRow(i);
                // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);

                // Add some text to the new cells:
                cell1.innerHTML = "<h4>" + item.producto + "</h4>";
                cell2.innerHTML = "<h4>" + item.Solicitado + "</h4>";
                cell3.innerHTML = '<input type="text" id = ' + item.producto + 'value="" readonly>';
                cell4.innerHTML = '<input type="text" value="" class="field left" onkeypress="enter(event)">';
                i++;
            });
            calcularDisponibles();
        }
    };
    xmlhttp.open("POST", "../controllers/pedidosplanta.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("destino=" + c);
}

function enter() {
    if (event.keyCode == 13 || event.which == 13) {
        calcularDisponibles();
    }
}


function calcularDisponibles() {
    var fecha = new Date().toISOString().substring(0, 10);
    var table = document.getElementById("disponible");
    var len = document.getElementById("disponible").rows.length;
    for (i = 1; i < len - 1; i++) {
        var row = table.rows[i];
        var producto = row.cells[0].innerHTML.substring(4, 10);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                const resultado = this.response;
                //alert(resultado);
                var p = resultado.substring(0,6)
                const input = document.getElementById(p);
                input.value = resultado.substring(6);
            }
        };
        xmlhttp.open("POST", "../controllers/disponibles.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("destino=" + planta+"&codigo="+producto+"&fecha="+fecha);
    }
}

function boton() {
    var table = document.getElementById("disponible");
    var len = document.getElementById("disponible").rows.length;
    var fecha = new Date().toISOString().substring(0, 10);
    for (i = 1; i < len - 1; i++) {
        var row = table.rows[i];
        var producto = row.cells[0].innerHTML.substring(4, 10);
        var estimado = row.cells[3].children[0].value;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.response);
            }
        };
        xmlhttp.open("POST", "../controllers/guardarEstimado.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("destino=" + planta+"&codigo="+producto+"&fecha="+fecha + "&estimado="+estimado);
    }

    alert("Guardado Correctamente");
    window.location.href = "../main.php";
}
