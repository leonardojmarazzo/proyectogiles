var i = 1;

function load() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("destino");

    var fecha = new Date().toISOString().substring(0,10);
    i = 1;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("idpedido").value = this.response;
            document.getElementById("fecha").value = fecha;
            document.getElementById("destino").value = c;
        }
    };
    xmlhttp.open("POST", "../controllers/nuevoPedido.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("destino="+c+"&fecha="+fecha);
    window.hinterXHR = new XMLHttpRequest();
}

function enter(event) {
    if (event.keyCode == 13 || event.which == 13) {
        nuevaFila();
    }
}

function nuevaFila(){
    var table = document.getElementById("pedido");
        i = document.getElementById("pedido").rows.length - 2;
        var row = table.insertRow(i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        cell1.innerHTML = '<input id="prod' + i + '"type="text" onkeyup="hinter()" list="huge_list' + i + '"><datalist id="huge_list'+i+'"></datalist>';
        cell2.innerHTML = '<input type="text" onkeypress="enter(event)">';
        cell3.innerHTML = '<select name="Unidad"><option value="UN">UN</option><option value="KG">KG</option></select>';
}


function hinter() {
    var id = "prod" + i
    // retireve the input element
    var input = document.getElementById(id);

    id = 'huge_list'+i;
    // retrieve the datalist element
    var huge_list = document.getElementById(id);

    // minimum number of characters before we start to generate suggestions
    var min_characters = 3;

    //console.log(input.value);

    if (input.value.length < min_characters ) { 
        return;
    } else { 
        
        // abort any pending requests
        window.hinterXHR.abort();

        window.hinterXHR.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                //console.log(this.response);

                // We're expecting a json response so we convert it to an object
                const response = JSON.parse( this.response ); 


                console.log(response);
                // clear any previously loaded options in the datalist
                huge_list.innerHTML = "";

                response.forEach(function(item) {
                    // Create a new <option> element.
                    var option = document.createElement('option');
                    option.value = item;

                    // attach the option to the datalist element
                    huge_list.appendChild(option);
                });
            }
        };

        window.hinterXHR.open("GET", "../controllers/hinterprods.php?query=" + input.value, true);
        window.hinterXHR.send()
    }
}


function guardarPedidos() {
    var table = document.getElementById("pedido");
    var len = document.getElementById("pedido").rows.length;
    var fecha = document.getElementById("fecha").value;
    var idpedido = document.getElementById("idpedido").value;
    for (i = 1; i < len - 2; i++) {
        var row = table.rows[i];
        var producto = row.cells[0].children[0].value.substring(0,6);
        var Cantidad = row.cells[1].children[0].value;
        var Unidad = row.cells[2].children[0].value;

        if (producto != '' && Cantidad != ''){
            var xmlhttp = new XMLHttpRequest();
            console.log(producto);
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(this.response);
                }
            };
            xmlhttp.open("POST", "../controllers/agregarProducto.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("fecha=" + fecha + "&id=" + idpedido + "&prod=" + producto + "&cant=" + Cantidad + "&unidad=" + Unidad);
        }
        
    }
    alert("Guardado Correctamente");
    window.location.href = "../main.php";
}