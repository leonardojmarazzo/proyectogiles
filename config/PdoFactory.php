<?php

require __DIR__.'/Config.php';

class PdoFactory
{
    public static function build($db = 'incidentes')
    {
        try {
            $dbConfig = self::getConfig();
            return new PDO(self::getDsn($db), $dbConfig->username, $dbConfig->password);
        } catch (PDOException $e) {
            echo "Error PDOException: " . $e->getMessage();
            die();
        }
    }

    private static function getDsn($db = 'incidentes')
    {
        $dbConfig = self::getConfig();
        if ($db == 'incidentes'){
            return "mysql:host=" . $dbConfig->host . ";dbname=" . $dbConfig->databasename;
        }else{
            return "mysql:host=" . $dbConfig->host . ";dbname=" . $dbConfig->databaseUsuarios;
        }
        
    }

    private static function getConfig()
    {
        return (new Config())->db;;
    }
}
