<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PCP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../style/main.css" />
    <script src="../JS/disponibles.js"></script>
    <script src="../JS/modal.js"></script>
</head>

<body onload="load()">
    <header>
        <div>
            <h1 class="header"><a href="../main.php">Programacion y control de la produccion - Disponibles planta</a></h1>
            <div class="header user-header">
                <h5><?php echo $usuario.' - '. $perfil ?></h5>
                <h5><a href="../logoff.php">Cerrar Sesion</a></h5>
            </div>   
        </div>
        <nav>
            <ul>
                <li class="dropdown">
                    <a class="menu">Faena</a>
                    <div class="dropdown-content">
                        <a href="../tropas.php">Tropas Faena</a>
                        <a href="../importarParteDiario.php">Importar Parte diario de Faena</a>
                    </div>
                </li>
                
                <li class="dropdown">
                    <a class="menu">Pedidos</a>
                    <div class="dropdown-content">
                        <a href="../pedidosComercial.php">Comercial</a>
                        <a href="../pedidoplanta.php?destino=Pilar">Pilar</a>
                        <a href="../pedidoplanta.php?destino=Varela">Varela</a>
                        <a href="../pedidoplanta.php?destino=Giles">Giles</a>
                    </div>
                </li>
                <li class="dropdown">
                    <a>Desposte</a>
                    <div class="dropdown-content">
                        <a href="../disponiblesplantas.php?destino=Giles">Disponibles - estimados Giles</a>
                        <a href="../disponiblesplantas.php?destino=Varela">Disponibles - estimados Varela</a>
                        <a href="../disponiblesplantas.php?destino=Pilar">Disponibles - estimados Pilar</a>
                        <a href="../diponiblesComercial.php">Disponibles comercial</a>
                        <a href="../tropasDespostadas.php">Tropas Despostadas</a>
                        <a href="../Corte.php">Exportar corte</a>
                    </div>
                </li>
                <li class="dropdown">
                    <a class="menu">Usuarios</a>
                    <div class="dropdown-content">
                        <a onClick="mostrar()">Crear Usuario</a>
                        <a onClick="mostrar2()">Cambiar contraseña</a>
                        
                    </div>
                </li>
            </ul>
        </nav>
    </header>

    <table id="disponible">
        <th>
            <h3>Producto</h3>
        </th>
        <th>
            <h3>Solicitado</h3>
        </th>
        <th>
            <h3>Disponible</h3>
        </th>
        <th>
            <h3>Estimado</h3>
        </th>
        <tr>
            <td class="boton" colspan="100%" onclick="boton()">Guardar</td>
        </tr>
    </table>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span id="close" onclick="cerrar()">&times;</span>
                <h2 id="tituloModal">Crear Usuario</h2>
            </div>
            <div>
                <h2>Nombre de Usuario</h2>
                <input name="UserName" id="user" placeholder="Nombre de usuario" required>
                <h2>Contraseña</h2>
                <input type="password" id="pass" name="Password" placeholder="Contraseña" required>
                <h2>Perfil</h2>
                <select name="Perfil" id="Perfil">
                    <option value="PCP">PCP</option>
                    <option value="Administrador">Administrador</option>
                    <option value="Comercial">Comercial</option>
                    <option value="Pilar">Pilar</option>
                    <option value="Varela">Varela</option>
                    <option value="Giles">Giles</option>
                    <option value="Faena">Faena</option>
                </select>
                <button type="button" class="btn" onclick="crearUsuario()">Guardar</button>
            </div>
        </div>

    </div>

    <div id="myModal2" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span id="close" onclick="cerrar()">&times;</span>
                <h2 id="tituloModal">Cambiar contraseña</h2>
            </div>
            <div>
                <h2>Contraseña actual</h2>
                <input name="UserName" id="passactual" placeholder="Contraseña actual" type="password" required>
                <h2>Nueva Contraseña</h2>
                <input type="password" id="passnueva" name="Password" placeholder="Contraseña Nueva" required>
                <h2>Nueva Contraseña</h2>
                <input type="password" id="passnueva2" name="Password" placeholder="Contraseña Nueva" required>
                <button type="button" class="btn" onclick="cambiarPass()">Guardar</button>
            </div>
        </div>
    </div>



</body>

</html>