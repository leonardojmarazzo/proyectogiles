<?php

require __DIR__.'/models/Producto.php';
//require __DIR__.'/config/PdoFactory.php';


function pedidoComercial($cod){
    $fecha = date('Y-m-d');
    $destino = 'Comercial';

    $sql = "SELECT Producto, Cantidad from pedidos p inner join productosxpedido pp on p.idPedido = pp.pedido and p.Fecha = pp.fecha where Producto = '$cod'";

//    echo $sql . PHP_EOL;

    $pdo = PdoFactory::build();
    $query = $pdo->prepare($sql);
    $query->execute();
    $cant = 0;
    while ($row = $query->fetch()){
        $cant += intval($row['Cantidad']);
    }
    return $cant;

}

function estimadoPlantas($cod){

    $prod = new Producto($cod);
    $fecha = date('Y-m-d');
    $est = $prod->getEstimado($fecha, 'Giles') + $prod->getEstimado($fecha, 'Varela') + $prod->getEstimado($fecha, 'Pilar');
    return $est;
}

function exportarCorte(){


    $filename = "Corte" . date('Ymd') . ".xls";

    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");
    $fecha = date('Y-m-d');
    echo "Codigo" . "\t" . "Descripcion" . "\t" . "Kilos\r\n";
    
    $sql = "SELECT Codigo, Descripcion from productos";

    //echo $sql;

    $pdo = PdoFactory::build();
    $query = $pdo->prepare($sql);
    $query->execute();


    while ($row = $query->fetch()){
        $codigo = $row['Codigo'];
        //echo $codigo;
        $estplantas = estimadoPlantas($codigo);
        $ped = pedidoComercial($codigo);
        $descripcion = $row['Descripcion'];
        $total = $estplantas + $ped;
        if ($total > 0){
            echo $codigo . "\t" . $descripcion . "\t" . $total . "\r\n";
        }
    }
}

session_start();

if (isset($_SESSION['perfil'])){
    if ($_SESSION['perfil'] == 'PCP' || $_SESSION['perfil'] == 'Administrador'){
        exportarCorte();
    }else{
        Header("Location: ../main.php");
    }
}else{
    require_once __DIR__.'/views/login.html';
}